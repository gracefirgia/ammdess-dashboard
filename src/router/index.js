import Vue from "vue";
import Home from "views/Home/Home.vue";
import Router from "vue-router";
import Login from "views/Login/Login.vue";
import Hello from "views/Hello/Hello.vue";
import Service from "views/Service/Service.vue";
import Tracking from "views/Tracking/Tracking.vue";
import History from "views/History/History.vue";
import store from "../vuex/store";

Vue.use(Router);

const routes = [{
        path: "/",
        name: "Login",
        component: Login,
        meta: {
            access: "free",
        },
    },
    {
        path: "/Home",
        name: "Home",
        component: Home,
        children: [{
                path: "/Hello",
                name: "Hello",
                component: Hello,
                meta: {
                    access: "limited",
                },
            },
            {
                path: "/service",
                name: "Service",
                component: Service,
                meta: {
                    access: "limited",
                },
            },
            {
                path: "/tracking",
                name: "Tracking",
                component: Tracking,
                meta: {
                    access: "limited",
                },
            },
            {
                path: "/history",
                name: "History",
                component: History,
                meta: {
                    access: "limited",
                },
            },
        ],
    },
];

const router = new Router({
    mode: "history",
    linkActiveClass: "active",
    routes,
});

router.beforeEach((to, from, next) => {
    /*
     *
     *  Check if access to destination url (page) is limited or not.
     *  If access limited (need login first) then will be redirected to login page
     *
     */
    console.log(to);
    if (
        (to.meta.access === "limited" && store.state.auth.token) ||
        to.meta.access === "free"
    ) {
        /*
         *
         * If He/She already has a token and want to access Login Page
         * Then will be redirected to Home page
         *
         */

        if (to.name === "Login" && store.state.auth.toker) {
            return next("/Hello");
        } else {
            return next();
        }
    } else {
        return next("/");
    }
});

export default router;