module.exports = [{
        type: 'item',
        isHeader: true,
        name: 'MAIN NAVIGATION'
    },
    {
        type: 'item',
        icon: 'fa fa-dashboard',
        name: 'Dashboard',
        router: {
            name: 'Hello'
        }
    },
    {
        type: 'item',
        icon: 'fa fa-eye',
        name: 'Tracking',
        router: {
            name: 'Tracking'
        }
    },
    {
        type: 'item',
        icon: 'fa fa-cogs',
        name: 'Service',
        router: {
            name: 'Service'
        }
    },
    {
        type: 'item',
        icon: 'fa fa-barcode',
        name: 'History',
        router: {
            name: 'History'
        }
    },



]
