import axios from "axios";

export default {
  login(userName, userSecret) {
    return axios
      .post(`${process.env.API_URL}/api/user/signin`, {
        userName: userName,
        userSecret: userSecret,
      })
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error));
  },
};
