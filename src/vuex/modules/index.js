/*
 *
 * Get the list of file names inside modules (this folder), except index.js
 * And then use that names as the name of modules
 *
 */

const files = require.context(".", false, /\.js$/);
const modules = {};

files.keys().forEach((key) => {
  if (key === "./index.js") return;
  modules[key.replace(/(\.\/|\.js)/g, "")] = files(key).default;
});

export default modules;
