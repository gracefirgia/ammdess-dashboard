import Axios from "axios";
import auth from "../api/services/auth";
import router from "../../router"
import Swal from "sweetalert2";

export default {
  namespaced: true,

  //* 'state' is all attributes that can be used within this file or outside this file as global variables
  //* you can use it directly on the inside or outside of this file
  //* but you shouldn't change the value from the outside of this file
  //* the only one that can change the value is 'commit'

  state: {
    status: "",
    token: localStorage.getItem("token") || "",
    userName: localStorage.getItem("userName") || "",
    userId: localStorage.getItem("userId") || "",
  },

  //* 'mutation' is bridge between 'state' (above) and 'action' (below)
  //* you shouldn't use it directly on the outside of this file

  mutations: {
    request(state) {
      state.status = "loading";
    },
    error(state) {
      state.status = "error";
    },
    success(state, payload) {
      state.status = "success";
      state.token = payload.token;
      state.userName = payload.userName;
      state.userId = payload.userId;
    },
    logout(state) {
      state.token = "";
      state.userName = "";
      state.userId = "";
    },
  },

  //* 'action' is bridge between 'mutation' (above) and 'outside' of this file (views folder)

  actions: {
    async login({ commit, getters }, payload) {
      try {
        commit("request");
        let response = await auth.login(payload.userName, payload.userSecret);
        if (response.status) {
          let token = response.data.token;
          let tokenContent = getters.getTokenContent(token);
          localStorage.setItem("token", token);
          localStorage.setItem("userName", tokenContent.userName);
          localStorage.setItem("userId", tokenContent.userId);

          commit("success", {
            token: token,
            userName: tokenContent.userName,
            userId: tokenContent.userId,
          });

          Axios.defaults.headers.common["Authorization"] = token;
        } else {
          commit("error");
        }
        return response;
      } catch (err) {
        commit("error");
        localStorage.removeItem("token");
        return err;
      }
    },
    logout({ commit }) {
      localStorage.clear();
      router.push("/");
      commit("logout");
    },
    checkTokenExpiration({dispatch},payload){
      if(payload.response.data.error.message === "jwt expired"){
        dispatch("logout");
      }
      Swal.fire({
        position: "top",
        icon: "error",
        html: "<b>Harap login kembali !</b>",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  },

  //* 'getter' only contains algorithm or function that does't use axios
  //* you can use it directly on the inside or outside of this file

  getters: {
    //* Decode JWT token into JSON format
    getTokenContent: (state) => (token) => {
      return JSON.parse(atob(token.split(".")[1]));
    },
  },
};
