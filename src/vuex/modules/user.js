const state = {
  main: {
    name: localStorage.getItem("userName") || "",
    position: "Admin",
    state: {
      color: "#3c763d",
      name: "Online",
    },
    createdAt: new Date(),
  },
};

const mutations = {

}

export default {
    state,
    mutations
}
