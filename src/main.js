// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './lib/css'
import './lib/script'
import './lib/global'

import Vue from 'vue'
import App from './App'
import router from './router'
import EventBus from './lib/eventBus.js'
import vuetify from './plugins/vuetify'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
import VueGeolocation from 'vue-browser-geolocation'
import * as VueGoogleMaps from 'vue2-google-maps'
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'
import Vuelidate from 'vuelidate'
import store from "./vuex/store";

Vue.prototype.$bus = EventBus
Vue.use(VueGeolocation)
Vue.use(Chartkick.use(Chart))
Vue.use(Vuelidate)

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDgGVA9hewwgncQJ4NYkwjSyvkNR8KmCQ8'
    },
    installComponents: false
})
Vue.component('GmapMap', VueGoogleMaps.Map)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    vuetify,
    store,
    template: '<App/>',
    components: {
        App,
    }
})
